//////////////////////////// Scroll to certain position ////////////////////////////

$(function () {
   $('a[href^="#"]').click(function () {

        let target = $(this).attr('href');

        $('html, body').animate({
            scrollTop: $(target).offset().top
        }, 1000)

   })
})


//////////////////////////// Scroll to top ////////////////////////////

const $btnToTop = $('<button id="to-top">To top</button>')

$(document.body).prepend($btnToTop)

$btnToTop.click(() => {
    $('html, body').animate({scrollTop: 0}, 1000)
});

$(window).scroll(function () {
    if (document.documentElement.scrollTop > window.screen.availHeight) {
            $btnToTop.css('display', 'block')
    } else {
        $btnToTop.css('display', 'none')
    }
})


//////////////////////////// slide toggle ////////////////////////////


const $hideShow = $('.hide-block')

$hideShow.click(() => {
    $('.most-popular').slideToggle('slow')
})



